extends: main.tpl
title: projects

---

Projects using the rusty-binder framework:

<dl>
<dt><a href="https://gitlab.com/rusty-binder/rusty-binder">rusty-binder</a></dt>
<dd>The generic framework for writing language bindings to Rust.</dd>
<dt><a href="https://gitlab.com/rusty-binder/rusty-cheddar">rusty-cheddar</a></dt>
<dd>A crate for generating C header files from Rust source files.<dd>
<dt><a href="https://gitlab.com/rusty-binder/steelix">steelix</a></dt>
<dd>A crate for generating Python bindings to Rust libraries.</dd>
<dt><a href="https://gitlab.com/rusty-binder/rustieR">rustieR</a></dt>
<dd>A crate for generating R bindings to Rust libraries.</dd>
<dt><a href="https://gitlab.com/rusty-binder/RATLAB">RATLAB</a></dt>
<dd>A crate for generating MATLAB bindings to Rust libraries.</dd>
</dl>
