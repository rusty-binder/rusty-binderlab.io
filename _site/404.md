extends: main.tpl
title: :(

---

# 404

## You've taken a wrong turn!

Either you've clicked on an outdated link, or the page you've come to has yet to be written. Please
[open an issue](https://gitlab.com/rusty-binder/rusty-binder.gitlab.io/issues/new) and I'll try to
get it sorted out for you.
