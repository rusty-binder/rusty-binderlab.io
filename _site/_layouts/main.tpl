<!DOCTYPE html>
<html>
<head>
    <title>
        {% if title %}
            {{ title }} |
        {% endif %}
        rusty-binder
    </title>

    <meta charset="UTF-8"/>
    <link href="/style/main.css" rel="stylesheet" type="text/css">
</head>
<body>
    <header>
        <h1><a href="/index.html">rusty-binder</a></h1>
        <nav>
            <ul>
                <li><a href="/projects.html">Projects</a></li>
                <li><a href="https://gitlab.com/rusty-binder">Source</a></li>
                <li><a href="/docs/index.html">Docs</a></li>
                <li><a href="/tutorial/index.html">Tutorial</a></li>
                <li><a href="/about.html">About</a></li>
            </ul>
        </nav>
    </header>

    <hr/>

    <main>
        {{ content }}
    </main>

    <hr/>

    <footer>
        This page was generated using <a href="https://github.com/cobalt-org/cobalt.rs">cobalt</a>.
    </footer>
</body>
</html>
