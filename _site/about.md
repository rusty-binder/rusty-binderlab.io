extends: main.tpl
title: about

---

Thoughts or suggestions for the website? Feel free to
[open an issue](https://gitlab.com/rusty-binder/rusty-binder.gitlab.io/issues/new) at the repo.

# Contributors

This is a list of people who have made contributions to one or more projects under the
[rusty-binder](https://gitlab.com/rusty-binder) umbrella.

- Sean Marshallsay
    - [Gitlab](https://gitlab.com/seamsay)
    - [Github](https://github.com/Sean1708)
- [Nicole Mazzuca](https://github.com/ubsan)
- [Matthew Gregan](https://github.com/kinetiknz)
