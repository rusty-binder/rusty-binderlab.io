# rusty-binder

[Introduction](./intro.md)

- [Usage](./usage.md)
- [Existing Compilers](./compilers.md)
    - [rusty-cheddar](./compilers/rusty-cheddar.md)
    - [steelix](./compilers/steelix.md)
    - [rustieR](./compilers/rustieR.md)
    - [RATLAB](./compilers/RATLAB.md)
- [rigor](./rigor.md)
- [Writing Your Own Compiler](./writing.md)
    - [An Example Crate](./writing/example.md)
    - [Start With C](./writing/c.md)
    - [C-Like Rust](./writing/c-like_rust.md)
    - [The `Compiler` Trait](./writing/trait.md)
    - [Utilities](./writing/utils.md)

[Glossary](./glossary.md)
[Acknowledgements](./acknowledgements.md)
