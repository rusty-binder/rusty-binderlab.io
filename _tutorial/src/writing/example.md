# An Example Crate

We're going to be using a very small 3-D vector maths crate in this tutorial so that we have
something concrete to actually apply these concepts to.

```rust
struct Vector {
    x: f64,
    y: f64,
    z: f64,
}

fn negate(vec: Vector) -> Vector {
    Vector {
        x: -vec.x,
        y: -vec.y,
        z: -vec.z,
    }
}

fn add(a: Vector, b: Vector) -> Vector {
    Vector {
        x: a.x + b.x,
        y: a.y + b.y,
        z: a.z + b.z,
    }
}

fn dot(a: Vector, b: Vector) -> f64 {
    let mut total = 0;
    total += a.x * b.x;
    total += a.y * b.y;
    total += a.z * b.z;
    total
}
```

As you can see it's very boring and we're actually ignoring most of the things that makes Rust
great, but that's only because we're focused on writing a compiler rather than a C API.
