# Writing Your Own Compiler

Rusty-Binder tries to make it very easy to write a new compiler for any language that can talk to C,
unfortunately this is still a very work in progress goal so hopefully this tutorial will be able to
walk you through the process in-depth.

This tutorial is going to walk you through the exact process that I used (minus most of the more
extreme pain points) when writing [steelix], the compiler for Python bindings. I actually wrote the
initial version of this tutorial alongside steelix, so you get to see my <del>complete and utter
failure</del> <ins>actual thought process</ins>.

[steelix]: https://gitlab.com/rusty-binder/steelix
